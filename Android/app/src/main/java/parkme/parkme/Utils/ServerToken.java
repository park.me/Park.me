package parkme.parkme.Utils;

import android.content.Context;

import java.io.Serializable;

import parkme.parkme.R;


/**
 * Created by deimi on 8/21/2017.
 */

public class ServerToken implements Serializable {
    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    private String access_token;
    private String token_type;
    private String refresh_token;

    static public void getAppAccessToken(Context context, ServerDataRetrieval.onResponseListener responseListener){
        String url = context.getString(R.string.apiUrl);
        url = url.concat(context.getString(R.string.apiLogin));
        String client_id = context.getString(R.string.appApiClientId);
        String client_secret = context.getString(R.string.appApiClientSecret);
        ServerDataRetrieval.getAppToken(context,url,client_id,client_secret, responseListener);
    }
    static public void getAppLoginAccessToken(Context context,String email,String password, ServerDataRetrieval.onResponseListener responseListener){
        String url = context.getString(R.string.apiUrl);
        url = url.concat(context.getString(R.string.apiLogin));
        String client_id = context.getString(R.string.appApiPassId);
        String client_secret = context.getString(R.string.appApiPassSecret);
        ServerDataRetrieval.getAppPasswordToken(url,email,password,client_id,client_secret,responseListener);
    }
}
