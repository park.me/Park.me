package parkme.parkme.Utils;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.Days;

import java.util.Date;

import parkme.parkme.R;

/**
 * Created by deimi on 04/01/2017.
 */

public class DateTimePickersFragments extends DialogFragment{

    public interface OnDateTimeSet{
        void onDateSelected(int year, int month, int day);
        void onTimeSelected(int hour, int minute);
    }

    public void setOnDateTimeSet(OnDateTimeSet onDateTimeSet) {
        this.onDateTimeSet = onDateTimeSet;
    }

    OnDateTimeSet onDateTimeSet;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_date_time_pickers,container,false);

        TextView title = (TextView) v.findViewById(R.id.fragment_date_time_pickers_title);
        final TimePicker timePicker = (TimePicker) v.findViewById(R.id.timePicker);
        final DatePicker datePicker = (DatePicker) v.findViewById(R.id.datePicker);
        final TextView submit = (TextView) v.findViewById(R.id.fragment_date_time_pickers_button_ok);
        TextView cancel = (TextView) v.findViewById(R.id.fragment_date_time_pickers_button_cancel);

        final Boolean date = getArguments().getBoolean("date");
        String message = getArguments().getString("title");
        long days = getArguments().getLong("days");
        timePicker.setIs24HourView(false);

        title.setText(message);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year = datePicker.getYear();

                int hour = timePicker.getCurrentHour();
                int minute = timePicker.getCurrentMinute();

                if(date){
                    onDateTimeSet.onDateSelected(year,month,day);
                }else {
                    onDateTimeSet.onTimeSelected(hour,minute);
                }
                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        if(date){
            datePicker.setVisibility(View.VISIBLE);
            timePicker.setVisibility(View.GONE);
            Date date1 = new Date();

            datePicker.setMinDate(date1.getTime() + getDays(days));
        }else{
            datePicker.setVisibility(View.GONE);
            timePicker.setVisibility(View.VISIBLE);
        }
        return v;
    }

    public long getDays(long days){
        return days * 86400000;
    }
}
