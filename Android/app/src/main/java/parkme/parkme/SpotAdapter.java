package parkme.parkme;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import parkme.parkme.SystemClases.Spot;

/**
 * Created by deimi on 8/29/2017.
 */

public class SpotAdapter extends RecyclerView.Adapter<SpotAdapter.ViewHolder> {
    private ArrayList<Spot> spots;

    public interface onClickListener{
        void onReserveClicked(String id);
        void onShowLocationClicked(double latitude, double longitude);
    }

    private onClickListener onClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ReserveSpotViewHolder reserveSpotViewHolder;
        public ViewHolder(View view){
            super(view);
            reserveSpotViewHolder = new ReserveSpotViewHolder();
            reserveSpotViewHolder.available = view.findViewById(R.id.reservation_image_avaibility);
            reserveSpotViewHolder.streetName = view.findViewById(R.id.reservation_text_street_name);
            reserveSpotViewHolder.currentFare = view.findViewById(R.id.reservation_text_current_fare);
            reserveSpotViewHolder.otherFare = view.findViewById(R.id.reservation_text_other_fare);
            reserveSpotViewHolder.overtimeFare = view.findViewById(R.id.reservation_text_fine_fare);
            reserveSpotViewHolder.showLocation = view.findViewById(R.id.reservation_text_show_location);
            reserveSpotViewHolder.reserve = view.findViewById(R.id.reservation_layout_reserve);
            reserveSpotViewHolder.faresLayout = view.findViewById(R.id.reservation_layout_fares);
            reserveSpotViewHolder.primaryInfo = view.findViewById(R.id.reservation_layout_primary_info);
            reserveSpotViewHolder.reserveImage = view.findViewById(R.id.reservation_image_reserve);
            reserveSpotViewHolder.mainLayout = view.findViewById(R.id.reservation_layout_main);
        }
    }

    public SpotAdapter(ArrayList<Spot> spots,onClickListener onClickListener){
        this.spots = spots;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View card_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_item_layout,parent,false);
        return new ViewHolder(card_item);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position){
        final Spot spot = spots.get(position);
        viewHolder.reserveSpotViewHolder.streetName.setText(spot.getStreet_name());
        viewHolder.reserveSpotViewHolder.streetName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onShowLocationClicked(spot.getLatitude(),spot.getLongitude());
            }
        });
        viewHolder.reserveSpotViewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onReserveClicked(spot.getId());
            }
        });
        viewHolder.reserveSpotViewHolder.faresLayout.setVisibility(View.VISIBLE);
        viewHolder.reserveSpotViewHolder.showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onShowLocationClicked(spot.getLatitude(),spot.getLongitude());
            }
        });
        viewHolder.reserveSpotViewHolder.reserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onReserveClicked(spot.getId());
            }
        });
        Drawable red = viewHolder.reserveSpotViewHolder.available.getContext().getDrawable(R.drawable.circle_red);
        if(!spot.isAvailable()){
            viewHolder.reserveSpotViewHolder.available.setBackground(red);
        }
        String fareFiat = " ETH / hr";

        viewHolder.reserveSpotViewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onReserveClicked(spot.getId());
            }
        });


        viewHolder.reserveSpotViewHolder.currentFare.setText(String.valueOf(spot.getCurrent_fare()).concat(fareFiat));
        viewHolder.reserveSpotViewHolder.otherFare.setText(String.valueOf(spot.getOther_fare()).concat(fareFiat));
        viewHolder.reserveSpotViewHolder.overtimeFare.setText(String.valueOf(spot.getOvertime_fare()).concat(fareFiat));

    }

    @Override
    public int getItemCount(){
        return spots.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }

}
