package parkme.parkme;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;

import org.w3c.dom.Text;

/**
 * Created by deimi on 8/30/2017.
 */

public class ReserveSpotViewHolder {
    public View available;
    public TextView streetName;
    public TextView currentFare;
    public TextView otherFare;
    public TextView overtimeFare;
    public TextView showLocation;
    public LinearLayout reserve;
    public LinearLayout faresLayout;
    public LinearLayout primaryInfo;
    public ImageView reserveImage;
    public TextView estimatedCost;
    public TextView fromTime;
    public TextView toTime;
    public TextView fare;
    public TextView estimatedTotalText;
    public LinearLayout mainLayout;
    public LinearLayout reservationLayout;
    public TextView elapsedTime;
}
