package parkme.parkme;

import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

import parkme.parkme.SystemClases.Reservation;
import parkme.parkme.SystemClases.Spot;

/**
 * Created by deimi on 8/31/2017.
 */

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ViewHolder>{

    private ArrayList<Reservation> reservations;

    public interface onClickListener{
        void onReserveClicked(String id);
        void onShowLocationClicked(double latitude, double longitude);
    }

    public interface reservationDone{
        void timerFinished(Reservation reservation);
    }

    public void setReservationDone(ReservationAdapter.reservationDone reservationDone) {
        this.reservationDone = reservationDone;
    }

    private ReservationAdapter.reservationDone reservationDone;
    private ReservationAdapter.onClickListener onClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ReserveSpotViewHolder reserveSpotViewHolder;
        public ViewHolder(View view){
            super(view);
            reserveSpotViewHolder = new ReserveSpotViewHolder();
            reserveSpotViewHolder.available = view.findViewById(R.id.reservation_image_avaibility);
            reserveSpotViewHolder.streetName = view.findViewById(R.id.reservation_text_street_name);
            reserveSpotViewHolder.currentFare = view.findViewById(R.id.reservation_text_current_fare);
            reserveSpotViewHolder.otherFare = view.findViewById(R.id.reservation_text_other_fare);
            reserveSpotViewHolder.overtimeFare = view.findViewById(R.id.reservation_text_fine_fare);
            reserveSpotViewHolder.showLocation = view.findViewById(R.id.reservation_text_show_location);
            reserveSpotViewHolder.reserve = view.findViewById(R.id.reservation_layout_reserve);
            reserveSpotViewHolder.faresLayout = view.findViewById(R.id.reservation_layout_fares);
            reserveSpotViewHolder.primaryInfo = view.findViewById(R.id.reservation_layout_primary_info);
            reserveSpotViewHolder.reserveImage = view.findViewById(R.id.reservation_image_reserve);
            reserveSpotViewHolder.estimatedCost = view.findViewById(R.id.parking_text_estimated_total);
            reserveSpotViewHolder.fare = view.findViewById(R.id.parking_text_daily_fare);
            reserveSpotViewHolder.toTime = view.findViewById(R.id.parking_text_to_time);
            reserveSpotViewHolder.fromTime = view.findViewById(R.id.parking_text_from_time);
            reserveSpotViewHolder.estimatedTotalText = view.findViewById(R.id.parking_text_estimated_total_text);
            reserveSpotViewHolder.mainLayout = view.findViewById(R.id.reservation_layout_main);
            reserveSpotViewHolder.reservationLayout = view.findViewById(R.id.reservation_layout_reservation_info);
            reserveSpotViewHolder.elapsedTime = view.findViewById(R.id.parking_text_elapsed_time);
        }
    }

    public ReservationAdapter(ArrayList<Reservation> reservations, ReservationAdapter.onClickListener onClickListener){
        this.reservations = reservations;
        this.onClickListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        View card_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_item_layout,parent,false);
        return new ViewHolder(card_item);
    }

    @Override
    public void onBindViewHolder(final ReservationAdapter.ViewHolder viewHolder, int position){
        final Reservation reservation = reservations.get(position);
        final Spot spot = reservation.getSpot();
        Drawable gray = viewHolder.reserveSpotViewHolder.available.getContext().getDrawable(R.drawable.circle_gray);
        Drawable blue = viewHolder.reserveSpotViewHolder.available.getContext().getDrawable(R.drawable.circle_blue);
        spot.setElapsedTimeText(viewHolder.reserveSpotViewHolder.elapsedTime);
        spot.setEstimatedTotalText(viewHolder.reserveSpotViewHolder.estimatedCost);
        if(reservation.isActive()){
            viewHolder.reserveSpotViewHolder.available.setBackground(blue);
            CountDownTimer countDownTimer = spot.getTimer();
            if(countDownTimer == null){
                spot.setOnTimeElapsedChanged(new Spot.onTimeElapsedChanged() {
                    @Override
                    public void timeChanged(String newTime) {
                        viewHolder.reserveSpotViewHolder.elapsedTime.setText(newTime);
                    }

                    @Override
                    public void totalChanged(float total) {
                        viewHolder.reserveSpotViewHolder.estimatedCost.setText(Float.toString(total));
                    }
                    @Override
                    public void timerFinished() {
                        reservation.setLeft(true);
                        reservationDone.timerFinished(reservation);
                    }
                });
                org.joda.time.Duration duration = new org.joda.time.Duration(reservation.getStart(),reservation.getEnd());
                spot.setupCountDownTimer(duration.getMillis(),1000);
            }
        }else{
            viewHolder.reserveSpotViewHolder.available.setBackground(gray);
        }
        viewHolder.reserveSpotViewHolder.estimatedCost.setText(String.valueOf(reservation.getTotal()));
        viewHolder.reserveSpotViewHolder.fromTime.setText(reservation.getStart().toString("dd/MM/yyyy hh:mm"));
        viewHolder.reserveSpotViewHolder.toTime.setText(reservation.getEnd().toString("dd/MM/yyyy hh:mm"));
        viewHolder.reserveSpotViewHolder.reservationLayout.setVisibility(View.VISIBLE);
        viewHolder.reserveSpotViewHolder.streetName.setText(spot.getStreet_name());
        viewHolder.reserveSpotViewHolder.streetName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onShowLocationClicked(spot.getLatitude(),spot.getLongitude());
            }
        });
        viewHolder.reserveSpotViewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.reserveSpotViewHolder.faresLayout.getVisibility() == View.GONE){
                    viewHolder.reserveSpotViewHolder.faresLayout.setVisibility(View.VISIBLE);
                }else{
                    viewHolder.reserveSpotViewHolder.faresLayout.setVisibility(View.GONE);
                }
            }
        });
        viewHolder.reserveSpotViewHolder.faresLayout.setVisibility(View.GONE);
        viewHolder.reserveSpotViewHolder.showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onShowLocationClicked(spot.getLatitude(),spot.getLongitude());
            }
        });

        String fareFiat = " ETH / hr";

        DateTime dateTime = new DateTime();

        if(dateTime.isAfter(reservation.getEnd())){
            viewHolder.reserveSpotViewHolder.estimatedTotalText.setText("Total:");
            viewHolder.reserveSpotViewHolder.estimatedCost.setText(String.valueOf(spot.roundTwoDecimals(spot.getTotal())).concat(" ETH"));
            viewHolder.reserveSpotViewHolder.elapsedTime.setText(spot.humanReadableTime());
        }


        viewHolder.reserveSpotViewHolder.fare.setText(String.valueOf(spot.getCurrent_fare()).concat(fareFiat));
        viewHolder.reserveSpotViewHolder.currentFare.setText(String.valueOf(spot.getCurrent_fare()).concat(fareFiat));
        viewHolder.reserveSpotViewHolder.otherFare.setText(String.valueOf(spot.getOther_fare()).concat(fareFiat));
        viewHolder.reserveSpotViewHolder.overtimeFare.setText(String.valueOf(spot.getOvertime_fare()).concat(fareFiat));

    }
    @Override
    public int getItemCount(){
        return reservations.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }
}
