package parkme.parkme.SystemClases;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by deimi on 8/31/2017.
 */

public class Reservation implements Serializable {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpot_id() {
        return spot_id;
    }

    public void setSpot_id(String spot_id) {
        this.spot_id = spot_id;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    private String id;
    private String spot_id;
    private DateTime start;
    private DateTime end;
    private boolean left;
    private float total;
    private Spot spot;

    public boolean isActive(){
        if(left){
            return false;
        };
        DateTime dateTime = new DateTime();
        return dateTime.isAfter(start) && dateTime.isBefore(end);
    }

    public Spot getSpot() {
        return spot;
    }

    public void setSpot(Spot spot) {
        this.spot = spot;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
