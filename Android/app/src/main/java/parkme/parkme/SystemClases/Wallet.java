package parkme.parkme.SystemClases;

import java.io.Serializable;

/**
 * Created by deimi on 8/30/2017.
 */

public class Wallet implements Serializable{
    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
