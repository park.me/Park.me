package parkme.parkme.SystemClases;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import java.math.BigDecimal;

/**
 * Created by deimi on 8/29/2017.
 */

public class Spot {

    public interface onTimeElapsedChanged{
        void timeChanged(String newTime);
        void totalChanged(float total);
        void timerFinished();
    }


    public void setOnTimeElapsedChanged(Spot.onTimeElapsedChanged onTimeElapsedChanged) {
        this.onTimeElapsedChanged = onTimeElapsedChanged;
    }

    private onTimeElapsedChanged onTimeElapsedChanged;

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getCurrent_fare() {
        return current_fare;
    }

    public void setCurrent_fare(float current_fare) {
        this.current_fare = current_fare;
    }

    public float getOther_fare() {
        return other_fare;
    }

    public void setOther_fare(float other_fare) {
        this.other_fare = other_fare;
    }

    public float getOvertime_fare() {
        return overtime_fare;
    }

    public void setOvertime_fare(float overtime_fare) {
        this.overtime_fare = overtime_fare;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    private String id;
    private float current_fare;
    private float other_fare;
    private float overtime_fare;
    private String street_name;
    private double latitude;
    private double longitude;
    private boolean available;
    private float total;

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public TextView getElapsedTimeText() {
        return elapsedTimeText;
    }

    public void setElapsedTimeText(TextView elapsedTimeText) {
        this.elapsedTimeText = elapsedTimeText;
    }

    private TextView elapsedTimeText;

    public TextView getEstimatedTotalText() {
        return estimatedTotalText;
    }

    public void setEstimatedTotalText(TextView estimatedTotalText) {
        this.estimatedTotalText = estimatedTotalText;
    }

    private TextView estimatedTotalText;

    public CountDownTimer getTimer() {
        return timer;
    }

    public String humanReadableTime(){
        String hours = "00";
        String minutes = "00";
        String seconds;
        int elapsed = (int) getElapsedTime();
        if(elapsed < 10){
            seconds = "0" + String.valueOf(elapsed);
        }else {
            seconds = String.valueOf(elapsed%60);
        }
        if(elapsed > 60){
            if(elapsed/60 < 10){
                minutes = "0" + String.valueOf(elapsed/60);
            }else {
                minutes = String.valueOf(elapsed/60);
            }
            if(elapsed< 10){
                seconds = "0" + String.valueOf(elapsed);
            }else {
                seconds = String.valueOf(elapsed%60);
            }
        }
        if(elapsed > 3600){
            if(elapsed/3600 < 10){
                hours = "0" + String.valueOf(elapsed/3600);
            }else {
                hours = String.valueOf(elapsed/3600);
            }
            if(elapsed < 10){
                seconds = "0" + String.valueOf(elapsed);
            }else {
                seconds = String.valueOf(elapsed%60);
            }
        }
        return hours + ":" + minutes + ":" + seconds;
    }

    public void setupCountDownTimer(long time, long step){
        CountDownTimer countDownTimer = new CountDownTimer(time,step) {
            @Override
            public void onTick(long l) {
                float elapsed = getElapsedTime()+1;
                float minuteFare = getCurrent_fare() / 60;
                float total = minuteFare * (elapsed / 60);
                Log.w("TOTAL",String.valueOf(total));
                setElapsedTime(elapsed);
                setTotal(total);
                elapsedTimeText.setText(humanReadableTime());
                if(roundTwoDecimals(total) > 0.0001){
                    estimatedTotalText.setText(String.valueOf(roundTwoDecimals(total)).concat(" ETH"));
                }else{
                    estimatedTotalText.setText("0.0001");
                }
                onTimeElapsedChanged.timeChanged(humanReadableTime());
            }

            @Override
            public void onFinish() {
                setAvailable(true);
                onTimeElapsedChanged.timerFinished();
            }
        };
        countDownTimer.start();
        setTimer(countDownTimer);
    }

    public float roundTwoDecimals(float d) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(5, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }


    public void setTimer(CountDownTimer timer) {
        this.timer = timer;
    }

    private CountDownTimer timer;

    public float getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(float elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    private float elapsedTime;
}