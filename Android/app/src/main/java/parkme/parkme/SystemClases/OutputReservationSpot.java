package parkme.parkme.SystemClases;

import java.io.Serializable;

/**
 * Created by deimi on 9/1/2017.
 */

public class OutputReservationSpot implements Serializable {
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIndexed() {
        return indexed;
    }

    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }

    private String sender;
    private String reciever;
    private int id;
    private boolean indexed;
}
