package parkme.parkme;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.protocol.parity.Parity;
import org.web3j.protocol.parity.ParityFactory;
import org.web3j.protocol.parity.methods.response.PersonalUnlockAccount;
import org.web3j.tx.Contract;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import parkme.parkme.SystemClases.Reservation;
import parkme.parkme.SystemClases.Spot;
import parkme.parkme.Utils.DateTimePickersFragments;
import parkme.parkme.Utils.ServerDataRetrieval;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Spot> nearbySpots;
    private ArrayList<Spot> frankfurtSpots;
    private ArrayList<Reservation> reservations = new ArrayList<>();
    private ArrayList<Reservation> pastParks = new ArrayList<>();

    private View walletsContent;
    private View parkingsContent;
    private View reservationsContent;
    private FrameLayout content;
    private BottomNavigationView navigation;
    private CoordinatorLayout reservationsMainLayout;

    private String walletAddress = "0x00fCcb2D0ab63B685E53534aD1b5Df905df2cc8a";
    private String walletPassword = "car1!";
    private String nodeUrl = "http://192.168.1.120:8545/";

    private String contractAddress = "0x3dd026Ff6ea43D82bC98ee50cc90614Ae213A4E2";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    drawParkingsView();
                    return true;
                case R.id.navigation_dashboard:
                    drawReservationsView();
                    return true;
                case R.id.navigation_notifications:
                    drawWalletsView();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        content = findViewById(R.id.content);

        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);

        reservationsContent = layoutInflater.inflate(R.layout.reservations,null);
        walletsContent = layoutInflater.inflate(R.layout.wallets,null);
        parkingsContent = layoutInflater.inflate(R.layout.parkings,null);

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        reservationsMainLayout = reservationsContent.findViewById(R.id.reservations_main_layout);

        generateFrankfurtRecylcerData();
        generateNearbyRecyclerData();

        navigation.setSelectedItemId(R.id.navigation_dashboard);
        connectToBlockchain();

    }

    private void drawReservationsView(){
        content.removeAllViews();
        setupReservationsView(reservationsContent);
        content.addView(reservationsContent);
    }

    private void setupReservationsView(final View reservationsView){
        final RecyclerView nearvySpots = reservationsView.findViewById(R.id.reservations_recycler_spots_nearby);

        nearvySpots.setHasFixedSize(true);
        nearvySpots.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        nearvySpots.setLayoutManager(layoutManager);


        SpotAdapter nearbySpotAdapter = new SpotAdapter(nearbySpots, new SpotAdapter.onClickListener() {
            @Override
            public void onReserveClicked(String id) {
                for (final Spot spot : nearbySpots){
                    if(spot.getId().equals(id)){
                        if(spot.isAvailable()){
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                    .setMessage("Do you want to reserve the spot for now or for later?")
                                    .setPositiveButton("NOW", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            reserveSpotNow(spot);
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setNegativeButton("LATER", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            reserveSpot(spot);
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).create();
                            alertDialog.show();
                            return;
                        }else{
                            reserveSpot(spot);
                            return;
                        }
                    }
                }
            }

            @Override
            public void onShowLocationClicked(double latitude, double longitude) {
                openMapsWithLocation(latitude,longitude);
            }
        });

        nearvySpots.setAdapter(nearbySpotAdapter);

//        RecyclerView nerbyReservationsRecycler = reservationsView.findViewById(R.id.reservations_recycler_reservations_nearby);
//        nerbyReservationsRecycler.setNestedScrollingEnabled(false);
//        RecyclerView.LayoutManager layoutManager3 = new LinearLayoutManager(MainActivity.this);
//        nerbyReservationsRecycler.setLayoutManager(layoutManager3);
//
//        ReservationAdapter reservationAdapter = new ReservationAdapter()

        RecyclerView frankfurtSpotsRecycler = reservationsView.findViewById(R.id.reservations_recycler_spots_frankfurt);

        frankfurtSpotsRecycler.setHasFixedSize(true);
        frankfurtSpotsRecycler.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(MainActivity.this);
        frankfurtSpotsRecycler.setLayoutManager(layoutManager1);

        SpotAdapter frankfurtSpotAdapter = new SpotAdapter(frankfurtSpots, new SpotAdapter.onClickListener() {
            @Override
            public void onReserveClicked(String id) {
                for (final Spot spot : frankfurtSpots){
                    if(spot.getId().equals(id)){
                        if(spot.isAvailable()){
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                    .setMessage("Do you want to reserve the spot for now or for later?")
                                    .setPositiveButton("NOW", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            reserveSpotNow(spot);
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setNegativeButton("LATER", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            reserveSpot(spot);
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setNeutralButton("CANCEL", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).create();
                            alertDialog.show();
                            return;
                        }else{
                            reserveSpot(spot);
                        }
                    }
                }
            }

            @Override
            public void onShowLocationClicked(double latitude, double longitude) {
                openMapsWithLocation(latitude,longitude);
            }
        });

        frankfurtSpotsRecycler.setAdapter(frankfurtSpotAdapter);
    }

    private void connectToBlockchain(){
        Log.w("WEB3J","Connecting to the server");
        try{
            Web3j web3j = Web3jFactory.build(new HttpService(nodeUrl));
            Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().sendAsync().get();
            Log.w("WEB3J",web3ClientVersion.getWeb3ClientVersion());
            unlockAccount(web3j);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void unlockAccount(Web3j web3j){
        try {
            Log.w("WEB3J","Start parity");
            Parity parity = ParityFactory.build(new HttpService(nodeUrl));
            PersonalUnlockAccount personalUnlockAccount = parity.personalUnlockAccount(walletAddress,walletPassword).sendAsync().get();
            if(personalUnlockAccount.accountUnlocked()){
                Log.w("WEB3J","Wallet Unlocked");
                getNonce(web3j);
            }else{
                Log.w("WEB3J","ERRRRROR");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void getNonce(Web3j web3j){
        try {
            Log.w("WEB3J","Get Nonce");
            EthGetTransactionCount getTransactionCount = web3j.ethGetTransactionCount(contractAddress, DefaultBlockParameterName.LATEST).sendAsync().get();

            BigInteger nonce = getTransactionCount.getTransactionCount();
            Log.w("WEB3J",nonce.toString());


        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void sendReservationToServer(){
        Log.w("SERVER","Request INIT");
        String url = "http://192.168.1.109:80/parkme/signalreservedempty";
        ServerDataRetrieval.JSONRequest(MainActivity.this, url, null, Request.Method.GET, null, new ServerDataRetrieval.onResponseListener() {
            @Override
            public void onRequestResponded(String response) {
                Log.w("SERVER",response);
            }

            @Override
            public void onStringResponded(String response) {
                Log.w("SERVER",response);
            }

            @Override
            public void onVolleyError(VolleyError volleyError) {

            }

            @Override
            public void onStringError(String error) {
                Log.w("SERVER",error);
            }
        });
    }


    private void reserveSpotNow(final Spot spot){
        DateTimePickersFragments dateTimePickersFragments = new DateTimePickersFragments();
        Bundle bundle = new Bundle();
        bundle.putString("title","End Date:");
        bundle.putBoolean("date",true);
        dateTimePickersFragments.setArguments(bundle);
        dateTimePickersFragments.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
            @Override
            public void onDateSelected(int year, int month, int day) {
                endYear = year;
                endMonth = month + 1;
                endDay = day;

                DateTimePickersFragments dateTimePickersFragments = new DateTimePickersFragments();
                Bundle bundle = new Bundle();
                bundle.putString("title","End Time:");
                bundle.putBoolean("date",false);
                dateTimePickersFragments.setArguments(bundle);
                dateTimePickersFragments.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
                    @Override
                    public void onDateSelected(int year, int month, int day) {

                    }

                    @Override
                    public void onTimeSelected(int hour, int minute) {
                        endHour = hour;
                        endMinute = minute;

                        DateTime endTime = new DateTime(endYear,endMonth,endDay,endHour,endMinute);
                        DateTime startTime = new DateTime();
                        reservations.add(createnewReservation(startTime,endTime,spot));
                        navigation.setSelectedItemId(R.id.navigation_home);
                        sendReservationToServer();
                        Toast.makeText(getApplicationContext(),"Reserved " + spot.getStreet_name() + " from: " + startTime.toString("dd/MM/yyyy hh:mm") + " to: " + endTime.toString("dd/MM/yyyy hh:mm"),Toast.LENGTH_LONG).show();
                    }
                });
                dateTimePickersFragments.show(getSupportFragmentManager(),"endTime");
            }

            @Override
            public void onTimeSelected(int hour, int minute) {

            }
        });
        dateTimePickersFragments.show(getSupportFragmentManager(),"endDate");
    }

    private void openMapsWithLocation(double latitude, double longitude){
        Uri gmmIntentUri = Uri.parse("http://maps.google.com/maps?q=loc:" + String.valueOf(latitude) + "," + String.valueOf(longitude));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void setupParkingsView(){
        final RecyclerView reservationsRecycler= parkingsContent.findViewById(R.id.parkings_recycler_current);
        reservationsRecycler.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        reservationsRecycler.setLayoutManager(layoutManager);

        ReservationAdapter reservationAdapter = new ReservationAdapter(reservations, new ReservationAdapter.onClickListener() {
            @Override
            public void onReserveClicked(String id) {

            }

            @Override
            public void onShowLocationClicked(double latitude, double longitude) {
                openMapsWithLocation(latitude,longitude);
            }
        });
        reservationAdapter.setReservationDone(new ReservationAdapter.reservationDone() {
            @Override
            public void timerFinished(Reservation reservation) {
                pastParks.add(reservation);
                reservations.remove(reservation);
                setupParkingsView();
            }
        });
        reservationsRecycler.setAdapter(reservationAdapter);

        RecyclerView reservationsUnactive = parkingsContent.findViewById(R.id.parkings_recycler_past);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(MainActivity.this);
        reservationsUnactive.setLayoutManager(layoutManager1);

        ReservationAdapter reservationAdapter1 = new ReservationAdapter(pastParks, new ReservationAdapter.onClickListener() {
            @Override
            public void onReserveClicked(String id) {

            }

            @Override
            public void onShowLocationClicked(double latitude, double longitude) {
                openMapsWithLocation(latitude,longitude);
            }
        });
        reservationsUnactive.setAdapter(reservationAdapter1);
    }

    private void drawParkingsView(){
        content.removeAllViews();
        setupParkingsView();
        content.addView(parkingsContent);
    }

    private void drawWalletsView(){
        content.removeAllViews();
        content.addView(walletsContent);
    }

    private void generateFrankfurtRecylcerData(){
        frankfurtSpots = new ArrayList<>();
        String[] strings = getResources().getStringArray(R.array.streetNames);
        ArrayList<String> streetNames =  new ArrayList<String>(Arrays.asList(strings));
        for(int cont=0; cont < 4 ; cont++){
            Spot spot = new Spot();
            spot.setId("lol" + String.valueOf(cont));
            spot.setStreet_name(streetNames.get(cont));
            spot.setCurrent_fare(cont + 3);
            spot.setOther_fare(cont + 2);
            spot.setOvertime_fare(cont + 5);
            spot.setAvailable(cont != 1);
            spot.setLatitude(50.139084 + cont);
            spot.setLongitude(8.737131 + cont);
            frankfurtSpots.add(spot);
        }
    }

    private void generateNearbyRecyclerData(){
        nearbySpots = new ArrayList<>();
        Spot spot = new Spot();
        spot.setId("1");
        spot.setAvailable(true);
        spot.setStreet_name("Gwinnerstraße 42, 60388 Frankfurt am Main");
        spot.setCurrent_fare((float) 0.3);
        spot.setOther_fare((float) 0.2);
        spot.setOvertime_fare((float) 0.6);
        spot.setLatitude(50.139084);
        spot.setLongitude(8.737131);
        nearbySpots.add(spot);
    }

    int startYear,startMonth,startDay,startHour,startMinute = 0;
    int endYear,endMonth,endDay,endHour,endMinute = 0;

    private void reserveSpot(final Spot spot){
        DateTimePickersFragments dateTimePickersFragments = new DateTimePickersFragments();
        Bundle bundle = new Bundle();
        bundle.putBoolean("date",true);
        bundle.putString("title","Start Date:");
        bundle.putLong("days",1);
        dateTimePickersFragments.setArguments(bundle);
        dateTimePickersFragments.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
            @Override
            public void onDateSelected(int year, int month, int day) {
                startYear = year;
                startMonth = month + 1;
                startDay = day;
                DateTimePickersFragments dateTimePickersFragments1 = new DateTimePickersFragments();
                Bundle bundle = new Bundle();
                bundle.putString("title","Start Time:");
                bundle.putBoolean("date",false);
                dateTimePickersFragments1.setArguments(bundle);
                dateTimePickersFragments1.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
                    @Override
                    public void onDateSelected(int year, int month, int day) {

                    }

                    @Override
                    public void onTimeSelected(final int hour, final int minute) {
                        startHour = hour;
                        startMinute = minute;

                        DateTimePickersFragments dateTimePickersFragments2 = new DateTimePickersFragments();
                        Bundle bundle = new Bundle();
                        bundle.putString("title","End Date:");
                        bundle.putBoolean("date",true);
                        bundle.putLong("days",1);
                        dateTimePickersFragments2.setArguments(bundle);
                        dateTimePickersFragments2.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
                            @Override
                            public void onDateSelected(int year, int month, int day) {
                                endYear = year;
                                endMonth = month + 1;
                                endDay = day;

                                DateTimePickersFragments dateTimePickersFragments3 = new DateTimePickersFragments();
                                Bundle bundle = new Bundle();
                                bundle.putString("title","End Time:");
                                bundle.putBoolean("date",false);
                                dateTimePickersFragments3.setArguments(bundle);
                                dateTimePickersFragments3.setOnDateTimeSet(new DateTimePickersFragments.OnDateTimeSet() {
                                    @Override
                                    public void onDateSelected(int year, int month, int day) {

                                    }

                                    @Override
                                    public void onTimeSelected(int hour, int minute) {
                                        endHour = hour;
                                        endMinute = minute;

                                        DateTime startTime = new DateTime(startYear,startMonth,startDay,startHour,startMinute);
                                        DateTime endTime = new DateTime(endYear,endMonth,endDay,endHour,endMinute);

                                        pastParks.add(createnewReservation(startTime,endTime,spot));
                                        navigation.setSelectedItemId(R.id.navigation_home);
                                        Toast.makeText(getApplicationContext(),"Reserved " + spot.getStreet_name() + " from: " + startTime.toString("dd/MM/yyyy hh:mm") + " to: " + endTime.toString("dd/MM/yyyy hh:mm"),Toast.LENGTH_LONG).show();
                                    }
                                });
                                dateTimePickersFragments3.show(getSupportFragmentManager(),"endTime");
                            }

                            @Override
                            public void onTimeSelected(int hour, int minute) {

                            }
                        });
                        dateTimePickersFragments2.show(getSupportFragmentManager(),"endDate");
                    }
                });
                dateTimePickersFragments1.show(getSupportFragmentManager(),"startTime");
            }

            @Override
            public void onTimeSelected(int hour, int minute) {

            }
        });
        dateTimePickersFragments.show(getSupportFragmentManager(),"startDate");
    }

    private Reservation createnewReservation(DateTime startTime, DateTime endTime,Spot spot){
        Duration duration = new Duration(startTime,endTime);
        float minuteFare = spot.getCurrent_fare() / 60;
        float total = minuteFare * duration.getStandardMinutes();
        spot.setAvailable(false);
        Reservation reservation = new Reservation();
        reservation.setStart(startTime);
        reservation.setEnd(endTime);
        reservation.setSpot(spot);
        reservation.setLeft(false);
        reservation.setTotal(total);
        return reservation;
    }
}
