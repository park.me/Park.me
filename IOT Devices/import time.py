import time
import grovepi
from grove_rgb_lcd import *
#import grovepi

# Connect the Grove Ultrasonic Ranger to digital port D4
# SIG,NC,VCC,GND
ultrasonic_ranger = 4

while True:
    try:
        # Read distance value from Ultrasonic
        setText(str(grovepi.ultrasonicRead(ultrasonic_ranger)))
        setRGB(0,128,0)
        time.sleep(.5)
    except TypeError:
        print ("Error")
    except IOError:
        print ("Error")
