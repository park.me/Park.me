pragma solidity ^0.4.15;

contract ParkMe {
    address private owner;

    uint public dayPrice;
    uint public nightPrice;
    uint public finePrice;

    uint[] reservationIds;

    mapping(uint => ReservationSpot) private reservation;

    struct ReservationSpot {
        address carAddress;
        uint startTime;
        uint endTime;
        uint planedCosts;
        uint receivedCosts;
        bool reserved;
    }

    //Events
    event OutputRequestSpot(address indexed sender, uint outStart, uint outEnd, uint outPlaned, uint outRecieved, bool reserved);
    event OutputReservationSpot(address indexed sender, uint outStart, uint outEnd, uint outPlaned, uint outRecieved, bool reserved);
    event OutputActualCosts(address indexed sender, uint outStart, uint outEnd, uint outPlaned, uint outActual, uint outRest);

    //Constructor

    function ParkMe(uint _dayPrice, uint _nightPrice, uint _finePrice) {
        owner = msg.sender;
        dayPrice = _dayPrice;
        nightPrice = _nightPrice;
        finePrice = _finePrice;
    }

    //Functions of the Contract

    function () payable {}

    function requestParkSpot(uint _reservationId, uint _startTime, uint _endTime) returns (bool success_, uint planedCosts_) {
        reservation[_reservationId].carAddress = msg.sender;
        reservation[_reservationId].startTime = _startTime;
        reservation[_reservationId].endTime = _endTime;
        reservation[_reservationId].planedCosts = calculateActualCosts(_reservationId, _endTime);
        reservation[_reservationId].receivedCosts = 0;
        reservation[_reservationId].reserved = false;
        success_ = true;

        planedCosts_ = reservation[_reservationId].planedCosts;
        
        OutputRequestSpot(msg.sender, reservation[_reservationId].startTime, reservation[_reservationId].endTime, reservation[_reservationId].planedCosts, reservation[_reservationId].receivedCosts, reservation[_reservationId].reserved);
        
        return (success_, planedCosts_);
    }

    function reservateParkSpot(uint _reservationId, uint _payedCosts) returns (bool success_) {
        reservation[_reservationId].receivedCosts = _payedCosts;
        if (reservation[_reservationId].receivedCosts == reservation[_reservationId].planedCosts * 2) {
            reservation[_reservationId].reserved = true;
        } else {
            //msg.sender.transfer(reservation[_reservationId].receivedCosts);
        }

        success_ = reservation[_reservationId].reserved;
       
        OutputReservationSpot(msg.sender, reservation[_reservationId].startTime, reservation[_reservationId].endTime, reservation[_reservationId].planedCosts, reservation[_reservationId].receivedCosts, reservation[_reservationId].reserved);

        return success_;
    }

    function chargeForParking(uint _reservationId, uint _endTime) {
        uint actualCosts;
        uint restCosts;

        reservation[_reservationId].endTime = _endTime;

        actualCosts = calculateActualCosts(_reservationId, _endTime);

        restCosts = reservation[_reservationId].receivedCosts - actualCosts;

        //owner.transfer(actualCosts);

        //reservation[_reservationId].carAddress.transfer(restCosts);

        OutputActualCosts(msg.sender, reservation[_reservationId].startTime, reservation[_reservationId].endTime, reservation[_reservationId].planedCosts, actualCosts, restCosts);
    }

    function calculateActualCosts(uint _reservationId, uint _endTime) returns(uint costs_) {
        uint parkingTime;
        uint dayTime;
        uint nightTime;
        uint fineTime;

        parkingTime = _endTime - reservation[_reservationId].startTime;

        uint dayCosts;
        uint nightCosts;
        uint fineCosts;

        if (reservation[_reservationId].startTime < 1800 && reservation[_reservationId].startTime >= 700) {
            dayTime = 1800 - reservation[_reservationId].startTime;
            if (reservation[_reservationId].endTime <= 1800) {
                dayTime = dayTime - (1800 - reservation[_reservationId].endTime); 
            }
        }

        if ((reservation[_reservationId].endTime > 1800 || reservation[_reservationId].endTime <= 700) && reservation[_reservationId].endTime <= _endTime) {
            if (reservation[_reservationId].endTime > 1800 ) {
                nightTime = reservation[_reservationId].endTime - 1800;
            }
            if (reservation[_reservationId].endTime <= 700) {
                nightTime = reservation[_reservationId].endTime - reservation[_reservationId].startTime;
            }
        }
        
        if (reservation[_reservationId].startTime < 700) {
            if (reservation[_reservationId].endTime > 700 ) {
                dayTime = dayTime + (reservation[_reservationId].endTime - 700);
                nightTime = nightTime + (700 - reservation[_reservationId].startTime);
            }
        }
        
        if (reservation[_reservationId].endTime > _endTime) {
            fineTime = _endTime - reservation[_reservationId].endTime;
        }
        
        dayCosts = dayTime * dayPrice;
        nightCosts = nightTime * nightPrice;
        fineCosts = fineTime * finePrice;

        costs_ = dayCosts + nightCosts + fineCosts;

        return costs_;
    }

    //only by owner functions

    function changeDayPrice (uint _dayPrice) onlyBy(owner) {
        dayPrice = _dayPrice;
    }

    function changeNightPrice (uint _nightPrice) onlyBy(owner) {
        nightPrice = _nightPrice;
    }
    
    function changeFinePrice (uint _finePrice) onlyBy(owner) {
        finePrice = _finePrice;
    }

    //modifier for auth 

    modifier onlyBy(address _account) {
        if (msg.sender != _account) {
            revert();
        }
        _;
    }

    //Destructor.

    function kill() onlyBy(owner) returns(uint) {
        selfdestruct(owner);
    }

}