//
//  main.cpp
//  Raspberry_c++
//
//  Created by Philipp Lang on 29.08.17.
//  Copyright © 2017 Philipp Lang. All rights reserved.
//

#include <iostream>
#include <stdlib.h>
#include <time.h>

int main()
{
    int countsec = 10; //runterzuzählende Zeit
    //hier dann die Eingabe von countsec
    bool flag = true;
    long start = time(NULL);
    do
    {
        if(time(NULL)>= start + countsec) flag = false;
        //hier optional noch die Ausgabe der bisher vergangenen Zeit (-> time(NULL) - start)
        //oder auch der noch verbleibenden (-> start + countsec - time(NULL)
    }
    while(flag);
    //hier dann die Soundausgabe oder whateve
        std::cout << "Hello, World!\n";
        return 0;
}
